import jax
import jax.numpy as jnp
import collections.abc


def get_device(device='cuda'):
    if 'cuda' in device and jax.devices('gpu'):
        device = 'cuda'
    else:
        device = 'cpu'
    return device


def dict_to_device(ob, device):
    if isinstance(ob, collections.abc.Mapping):
        return {k: dict_to_device(v, device) for k, v in ob.items()}
    else:
        return jax.device_put(ob, device)


