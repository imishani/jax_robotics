import unittest

import jax.numpy as jnp

from jax_robotics.jax_utils import rotations


class TestUtils(unittest.TestCase):

    def test_q_mul(self):
        self.assertTrue((rotations.q_mul(jnp.array([[1, 2, 3, 4],
                                                    [1, 0, 0, 0]]),
                                         jnp.array([[5, 6, 7, 8],
                                                    [0, 1, 0, 0]])) ==
                         jnp.array([[-60, 12, 30, 24],
                                    [0, 1, 0, 0]])).all())


if __name__ == '__main__':
    unittest.main()
