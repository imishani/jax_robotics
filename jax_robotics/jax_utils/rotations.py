import timeit

import jax.numpy as jnp
from jax import vmap, jit, random


def sqrt_with_mask(x: jnp.ndarray) -> jnp.ndarray:
    """
    :param x: input jax array
    :return: all positive elements of x (jnp.sqrt(jnp.maximum(0, x))
    """
    return jnp.sqrt(jnp.maximum(0, x))


def q_exp_map(v: jnp.ndarray,
              base: jnp.ndarray = None) -> jnp.ndarray:
    """

    :param v:
    :param base:
    :return:
    """
    pass


def single_q_mul(p_, q_):
    return jnp.concatenate([jnp.array([p_[0] * q_[0] - jnp.dot(p_[1:], q_[1:])]),
                            p_[0] * q_[1:] + q_[0] * p_[1:] + jnp.cross(p_[1:], q_[1:])])


@jit
def q_mul(p: jnp.ndarray, q: jnp.ndarray) -> jnp.ndarray:
    """
    This function multiplies two quaternions using the Hamilton product.
    :param p: quaternion p (batch_size, 4) or (4,). The first element is the scalar part.
    :param q: quaternion q (batch_size, 4) or (4,). The first element is the scalar part.
    :return:
    """
    if len(p.shape) == 1:
        # p is a single quaternion. reshape it.
        if len(q.shape) == 1:
            # q is also a single quaternion. reshape it.
            p = p.reshape(1, 4)
            q = q.reshape(1, 4)
        else:
            # q is a batch of quaternions. Broadcast p.
            p = p.reshape(q.shape[0], 4)
    else:
        # p is a batch of quaternions. Broadcast q.
        if len(q.shape) == 1:
            q = q.reshape(1, 4)
        else:
            # Both p and q are batches of quaternions. Check that they have the same batch size.
            assert p.shape[0] == q.shape[0]
    return vmap(single_q_mul, (0, 0), 0)(p, q)



